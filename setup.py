from setuptools import setup,find_packages
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt","r") as req:
    requirements = req.readlines()

requirements = [x.strip() for x in requirements]
setuptools.setup(
    name="sajilo_cv",
    version="0.0.1",
    author="Shushant Pudasaini",
    author_email="shusrulz@gmail.com",
    description="Package for parsing and scoring resumes",
    packages = find_packages(),
    zip_safe = False,
    long_description=long_description,
    long_description_content_type="text/markdown",
    include_package_data = True,
    url="https://gitlab.com/shusrulz/sajilo_cv",
    install_requires = requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
