import os

ROOT = os.path.dirname(os.path.abspath(__file__))
word2vec_model_path = os.path.join(ROOT,'cvscorer/models/word2vec')
tempStorage = os.path.join(ROOT,'tempStorage')



'''<------------Directory Absolute Paths------------------------------>'''
RootDir = os.path.dirname(os.path.abspath(__file__))
ParserDir = os.path.join(RootDir,'cvparser')
ScorerDir = os.path.join(RootDir,'cvscorer')
CommonDir = os.path.join(RootDir,'common')


'''<------------------------Paths------------------------------------->'''
'''<--------for parser only -------------->'''
ResumeIdentifierModelPath = os.path.join(ParserDir,'models/Resume_Identification_Model.h5')
ResumeSegmentationModelPath = os.path.join(ParserDir,'models/segment_identifier.pkl')
MaleFemaleIdentifierModelPath = os.path.join(ParserDir,'models/male_female_identifier.pickle')

'''<--------for scorer only--------------->'''
Word2vecModelPath =os.path.join(ScorerDir,'models/model.bin')

'''<---------for both/common usage --------->'''

jarPath = os.path.join(CommonDir,'stanfordNER/stanford-ner.jar')
NerModelPath = os.path.join(CommonDir,'stanfordNER/NER_model.ser.gz')

'''<---------------------Preprocessors-------------------------------->'''
'''<---------for parser only--------------->'''
MaleFemaleTokenizer = ParserDir + '/models/male_female_tokenizer.pickle'


'''<-------------------Tuning Parameters------------------------------>'''
ResumeIdentifierThreshold = 0.0468

'''<--------------------pandas database ------------------------------->'''
Languagefiles = os.path.join(ParserDir,'datahouse/languages.csv')
skillFilePath = os.path.join(ROOT,'datastore/technical_skills.csv')
softskillFilePath = os.path.join(ParserDir,'datahouse/soft_skills_resume.csv')
Nationalityfiles = os.path.join(ParserDir,'datahouse/nationality_data.csv')

"<---------------------Write to file ----------------------------------->"
Write2file = True
