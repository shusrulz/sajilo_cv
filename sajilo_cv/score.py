import os
import json
import math
from datetime import datetime
import logging
import ast
import concurrent.futures
from contextlib import contextmanager

import spacy
from geopy.geocoders import Nominatim 
from geopy.distance import geodesic
import pandas as pd
import numpy as np

from sajilo_cv.configuration import tempStorage
from sajilo_cv.datareader import prepare_text,prepare_text_from_string
from sajilo_cv.cvscorer.helper import PreprocessData,NLTKHelper
from sajilo_cv.cvscorer.rankword2vec import VectorScorer
from sajilo_cv.cvparser.parse import Parser


logger = logging.getLogger(__name__)
# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('file.log')
c_handler.setLevel(logging.DEBUG)
f_handler.setLevel(logging.ERROR)
# Create formatters and add it to handlers
c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)
# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)


geolocator = Nominatim()

resume_parser_obj = Parser()
preprocessor_obj = PreprocessData()
preprocessor_obj_for_score = PreprocessData()
score_generator_obj= VectorScorer()
helper_obj = NLTKHelper({"experience_type_a:{<VB>?<CD>?<N.*>?<IN>?<N.*|VBN|JJ>+<IN|VB.*>+<DT>?<JJ|VBG|N.*>+<CC>?<N.*>?}"})

class Scorer:
	def prepare_profile(self,profile):
		'''
		Takes user profile and filters
		only the necessary attributes
		for the score calculation
		:arg profile of user :type dict
		:returns technical skills,
		all_experiences :type set , list
		'''
		technical_skills = set(profile.get('technical_skills'))
		soft_skills = set(profile.get('soft_skills'))
		skills = technical_skills.union(soft_skills)
		# technical_skills = set(profile.get('skills'))
		designations= []
		try:
			job_seeker_location = json.loads(profile['profile'])['address']['city']
		except:
			job_seeker_location = profile['profile']['address']['city']
		all_experiences =[]
		designation_dates = []
		for xp in json.loads(profile.get('exps')):
			designation = xp.get('designation')
			entry_date = xp.get('entry_date')
			exit_date = xp.get('exit_date')
			if not entry_date:
				entry_date = datetime.today().strftime('%Y-%m-%d')
			if not exit_date:
				exit_date = datetime.today().strftime('%Y-%m-%d')
			entry_year = entry_date.split("-")[0]
			exit_year = exit_date.split("-")[0]
			years_of_exp =  int(exit_year) - int(entry_year)
			designations.append(designation)
			all_experiences.append((str(years_of_exp)+ ' years of'+
									' experience as '+designation))
			designation_dates.append((entry_date,designation))
			set(designations)
		return skills,all_experiences,designations,job_seeker_location,designation_dates

	def prepare_job_description(self,job_description):
		'''
		Takes the job description and
		extracts the skills and experience
		that falls under the job requirements
		:arg job_description
		:type str
		:returns required_skills,required_experiences
		:type set ,list
		'''
		_ = preprocessor_obj_for_score.preprocess_text(job_description)
		n_grams = preprocessor_obj_for_score.n_grams
		tagged_tokens = preprocessor_obj_for_score.tagged_tokens
		required_skills = helper_obj.skillsfinder(n_grams)
		required_experience = helper_obj.experiencefinder(tagged_tokens)
		return required_skills,required_experience

	def calculate_score(self,file,job_descriptions,dolower = False,state = 'PRODUCTION'):
		cleaned_tokenized_resume = prepare_text(file,dolower=dolower)
		final_data = preprocessor_obj.preprocess_text(cleaned_tokenized_resume)
		# resume_skills = helper_obj.skillsfinder(preprocessor_obj.n_grams)
		resume_vector = score_generator_obj.create_vector(final_data)
		my_score = {}
		if state=='DEBUG':
			for job_description in [job_descriptions]:
				my_score =0
				job_text = prepare_text_from_string(job_description)
				final_job = preprocessor_obj.preprocess_text(job_text)
				# job_skills = helper_obj.skillsfinder(preprocessor_obj.n_grams)
				job_vector = score_generator_obj.create_vector(final_job)
				vec_sim = score_generator_obj.calculate_similarity(job_vector, [resume_vector])
				try:
					my_score = int(vec_sim * 100)
				except Exception as e:
					logger.exception(e)
			return my_score

		elif state =="PRODUCTION":
			for job_id,job_description in job_descriptions.items():
				job_text = prepare_text_from_string(job_description)
				final_job = preprocessor_obj.preprocess_text(job_text)
				# job_skills = helper_obj.skillsfinder(preprocessor_obj.n_grams)
				job_vector = score_generator_obj.create_vector(final_job)
				vec_sim = score_generator_obj.calculate_similarity(job_vector,[resume_vector])
				if math.isnan(vec_sim):
					vec_sim = 0
				else:
					pass
				try:
					my_score[job_id] = int(vec_sim*100)
				except Exception as e:
					logger.exception(e)
			return my_score

	def calculate_distance(self,address1,address2):
		''' 
		Gives distance between two locations
		params: address of first location
				adress of second location
		returns: distance between two address in kilometers
		''' 
		location1,(latitude1, longitude1) = geolocator.geocode(address1)
		location2,(latitude2, longitude2) = geolocator.geocode(address2) 
		distance = geodesic((latitude1, longitude1), (latitude2, longitude2)).kilometers
		return distance

	def calculate_progress(self,designation_dates):
		unique_data = list(set(designation_dates))
		uni_designations = []
		uni_dates = []
		for i in range(len(unique_data)):
			uni_dates.append(unique_data[i][0])
			uni_designations.append(unique_data[i][1])

		# add classifier here to normalize designation
		normailized_design = ['Junior Employee','Employee','Employee','Senior Employee']
		
		corres_num = []
		for i in normailized_design:
			if(i == 'Intern'):
				corres_num.append(0)
			elif(i=='Junior Employee'):
				corres_num.append(1)
			elif(i=='Employee'):
				corres_num.append(2)
			elif(i=='Senior Employee'):
				corres_num.append(3)

		list_data = []
		for i in range(len(uni_dates)):
			list_data.append((uni_dates[i],corres_num[i]))

		df = pd.DataFrame(list_data, columns=['Column1', 'Column2'])

		df['date_ordinal'] = pd.to_datetime(df['Column1']).map(datetime.toordinal)
		del df['Column1']

		together_conv = df.values.tolist()

		min_max_scaler = MinMaxScaler()

		norm_data = min_max_scaler.fit_transform(together_conv)
		final_norm = pd.DataFrame(norm_data, columns=['value','normalized_date'])

		slope, intercept, r_value, p_value, std_err = stats.linregress(final_norm['normalized_date'], final_norm['value'])
		slope = slope - 0.0089661424386942
			# print(std_err)
		# slope ranges from (+1.0089661424386942 to -1.0089661424386942)

		if(slope>0):
			progress_score = slope * 15
			print('Progress Score :',progress_score)
		else:
			progress_score = 0
			print("Since your progress isn't promising your Progress Score is ",progress_score)

		return progress_score 

	def calculate_score_for_profile(self,profile,job_title,req_exp,req_skills,employer_location):
		user_id = profile.get('user_id')
		user_skills,user_exp,designations,user_location, designation_dates = self.prepare_profile(profile)
		user_skills = [x.lower() for x in user_skills]
		req_skills = [x.lower() for x in req_skills]
		matched_skills = set(user_skills).intersection(set(req_skills))

		#calculating score for distance
		try:
			distance = self.calculate_distance(employer_location,user_location)
			#Normalizing to a range from 0 to 10
			rounded_dist = round(distance)
			if(rounded_dist > 0):
				distance_score = 5 - (distance/20000*5)
			else:
				distance_score = 5
		except:
			distance_score = 5

		#calculating score for skills
		try:
			skill_score = len(matched_skills)/len(req_skills) *25
		except:
			skill_score = 25

		
		#calculating score for job designation    
		if job_title in designations:
			desig_score = 40
		else:
			vector_score = []
			for desig in designations:
				desig_vec = score_generator_obj.create_vector(desig)
				job_title_vec = score_generator_obj.create_vector(job_title)
				desig_job_title_similarity_desig = score_generator_obj.calculate_similarity(desig_vec,[job_title_vec])
				vector_score.append(desig_job_title_similarity_desig)
			try:
				#Calculating average score for every designations
				desig_job_title_similarity_designations = sum(vector_score)/len(designations)
			except:
				desig_job_title_similarity_designations = 0
			#Normalizing to range 0 to 40
			desig_score = desig_job_title_similarity_designations * 40    

		#calculating scores for experience
		if len(req_exp) == 0 or len(user_exp) == 0:
			vec_sim = 0
		else:
			req_exp_vector = score_generator_obj.create_vector(req_exp)
			user_exp_vector = score_generator_obj.create_vector(user_exp)

			vec_sim = score_generator_obj.calculate_similarity(req_exp_vector,[user_exp_vector])
		
		#Normalizing to a range from 0 to 40
		experience_score = vec_sim * 15

		if np.isnan(experience_score):
			experience_score = 0
		if np.isnan(desig_score):
			desig_score = 0
		if np.isnan(skill_score):
			skill_score = 0

		try:
			if (not designation_dates):
				progress_score = 0
			else:
				progress_score = self.calculate_progress(designation_dates)
		except:
			progress_score = 15

		total_score = experience_score + desig_score + skill_score + distance_score + progress_score
		return user_id,total_score

	def calculate_score_for_job_descriptions(self,job,designations,user_exp,user_skills,user_location, designation_dates):
		job_id = job.get('id')
		job_title = job.get('job_title')
		job_description = job.get('job_description')
		req_skills,req_experience = self.prepare_job_description(job_description)
		employer_location = job['location']['city']
		
		try:
			distance = self.calculate_distance(employer_location,user_location)
			#Normalizing to a range from 0 to 10
			distance_score = 5 - ((distance/20000)*5)
		except:
			distance_score = 5

		
		matched_skills = user_skills.intersection(req_skills)
		try:
			skill_score = len(matched_skills)/len(req_skills) *25
		except:
			skill_score = 25


		if job_title in designations:
			desig_score = 40
		else:
			vector_score = []
			for desig in designations:
				desig_vec = score_generator_obj.create_vector(desig)
				job_title_vec = score_generator_obj.create_vector(job_title)
				desig_job_title_similarity_desig = score_generator_obj.calculate_similarity(desig_vec,[job_title_vec])
				vector_score.append(desig_job_title_similarity_desig)
			
			#Calculating average score for every designations
			desig_job_title_similarity_designations = sum(vector_score)/len(designations)
			#Normalizing to range 0 to 40
			desig_score = desig_job_title_similarity_designations * 40

		#In case the chunking didn't extract any experience from job description 
		if len(req_experience) == 0:
			req_experience = ['Minimum 1 years of experience as ' + job_title]

		req_exp_vector = score_generator_obj.create_vector(req_experience)
		user_exp_vector = score_generator_obj.create_vector(user_exp)

		vec_sim = score_generator_obj.calculate_similarity(req_exp_vector,[user_exp_vector])
		
		#Normalizing to a range from 0 to 40
		experience_score = vec_sim * 15

		try:
			if (not designation_dates):
				progress_score = 0
			else:
				progress_score = self.calculate_progress(designation_dates)
		except:
			progress_score = 15

		total_score = experience_score + desig_score + skill_score + distance_score + progress_score
		
		return job_id,total_score

	def get_score_from_job_descriptions(self,json_data):
	    form_data_ = json_data
	    job = ast.literal_eval(form_data_.get('job'))
	    job_id = job.get('id')
	    job_title = job.get('job_title')
	    job_description = job.get('job_description')
	    employer_location = job['location']['city']
	    req_skills,req_experience = self.prepare_job_description(job_description)
	    user_profiles = form_data_.get('user_profiles')

	    #In case the chunking didn't extract any experience from job description 
	    if len(req_experience) == 0:
	        req_experience = 'Experience in ' + job_title
	    
	    #Multiprocessing because of heavy computational time
	    with concurrent.futures.ProcessPoolExecutor() as executor:
	        results = [executor.submit(self.calculate_score_for_profile,profile,job_title,req_experience,req_skills,employer_location) for profile in user_profiles]
	    my_score = {}
	    
	    for f in concurrent.futures.as_completed(results):
	        id,total_score = f.result()
	        my_score[id] = total_score
	    return my_score

	def get_score_from_form(self,json_data):
		form_data_ = json_data
		user_profile = form_data_.get('user_profile')
		user_skills,user_exp,designations,user_location,designation_dates = self.prepare_profile(user_profile)
		my_score = {}
		job_descriptions = list(json.loads(form_data_.get('jobs')))
		
		
		#Multiprocessing because of heavy computational time
		with concurrent.futures.ProcessPoolExecutor() as executor:
			result = [executor.submit(self.calculate_score_for_job_descriptions,job_description,designations,user_exp,user_skills,user_location, designation_dates) for job_description in job_descriptions]
		my_score = {}
		
		for f in concurrent.futures.as_completed(result):
			id,total_score = f.result()
			my_score[id] = total_score

		return my_score

	def get_score_from_resume(self,resumepath,jd_json):
		score = self.calculate_score(resumepath,jd_json)
		return score