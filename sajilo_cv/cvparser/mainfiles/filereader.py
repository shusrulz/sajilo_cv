import os
import re

from subprocess import Popen,PIPE
# import Image
from pytesseract import image_to_string
import docx2txt
from PIL import Image
from tika import parser


import os
import re
import docx2txt
import fitz
import logging

logger = logging.getLogger(__name__)

# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('file.log')
c_handler.setLevel(logging.DEBUG)
f_handler.setLevel(logging.ERROR)

# Create formatters and add it to handlers
c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)



def clean_text(text,dolower = False):
    '''
    accepts the plain text and makes
    use of regex for cleaning the noise
    :param: text :type:str
    :return:cleaned text :type str
    '''
    '''
    function that takes the text containing noise
    and tries to clean it as much as possible
    :param text: str
    :return: cleaned text of type str
    '''
    text = re.sub(r' +',' ', text)
    text = re.sub(r'\t',' \n, ', text)
    text = re.sub(r'\n+',' \n ,', text)
    text = re.sub(r',+',',', text)
    text = text.encode('ascii', errors='ignore').decode("utf-8")
    cleaned_lines = [re.sub(r'^[\W]+','',line) for line in text.split("\n")]
    text = '\n'.join(cleaned_lines)
    return text


def pdf_to_text(file_path, dolower):
    '''
    function that takes datapath
    extracts the plain text from pdf
    for training the word to vec model
    :param file_path :type str
    :return:text   :type str
    '''
    doc = fitz.open(file_path)
    number_of_pages = doc.pageCount
    text = ''
    for i in range(0, number_of_pages):
        page = doc.loadPage(i)
        pagetext = page.getText("text")
        text += pagetext
    text = clean_text(text, dolower)
    return text

def docx_to_text(file_path,dolower):
    '''
    function for extracting plain text
    from the docx files
    :param file_path :type str
    :return:text     :type str
    '''
    text = ""
    text += docx2txt.process(file_path)
    text = clean_text(text,dolower)
    return text


def txt_to_text(self, file_path,dolower):
    '''
    Extracts plain text from
    txt files
    :param file_path :type str
    :return:text     :type str
    '''
    text = ""
    with open(file_path, mode='r', encoding='unicode_escape', errors='strict', buffering=1) as file:
        data = file.read()
    text += data
    text = self.clean_text(text,dolower)
    return text

def prepare_text_from_string(text):
    '''
    takes string of text and then
    cleans the noise
    :param: text      :type str
    :return: cleaned_text :type str
    '''
    cleaned_text = clean_text(text)
    return cleaned_text

def prepare_text(file,dolower = False):
    '''
    converts the plain text into the form
    that is compatible for training the
    word2vec model
    :param: file :type str
    :return: cleaned tokenized sentences :type list
    '''
    reader_choice ={'.pdf': pdf_to_text,
                    '.docx': docx_to_text,
                    '.txt': txt_to_text}
    path, ext = os.path.splitext(file)
    try:
        file_content = reader_choice[ext](file,dolower = dolower)
        return file_content
    except Exception as e:
        logging.error(e)








#
#
# training_data = W2vData()
# sentences = training_data.prepare_training_text()
# print(sentences)


def doc_to_text(filepath):
    '''
    This function takes the doc file
    from the file path param and returns
    the cleaned the text from the file.
    :param filepath: path/directory of the doc file in the system
    :return: Returns the cleaned text from the file
    '''
    text = ""
    cmd = ['antiword', filepath]
    p = Popen(cmd, stdout=PIPE)
    stdout, stderr = p.communicate()
    text += stdout.decode('utf-8', 'ignore')
    text = clean_text(text)
    return text

def img_to_text(filepath):
    '''
    This function takes the image file
    from the file path param and returns
    the cleaned the text from the  image file.
    :param filepath: path/directory of the image file in the system
    :return: Returns the cleaned text from the image file
    '''
    text = image_to_string(Image.open('test.png'))
    text = clean_text(text)
    return text

def read_cv(file_path):
    '''
    This function identifies the file extensions
    of the cv and will parse the text from those file
    according to the extensions using the other file
    parsing methods and returns the cleaned text accordingly.
    :param file_path: path/directory of the file in the system to be parsed
    :return: returns the text(str) with respect to file extentions.
    '''

    image_extensions=['.jpeg','.png','.jpg','.psd','.ai']
    _,file_extension=(os.path.splitext(file_path))
    if file_extension.lower() in image_extensions:
            file_extension='.img'
    else:
        pass

    options={
            '.pdf':pdf_to_text,
            '.docx':docx_to_text,
            '.txt':txt_to_text,
            '.doc':doc_to_text,
            '.img':img_to_text,
            }
    try:
        text=options[file_extension](file_path)
        return text
    except Exception as e:
        print("Exception at fileReader:"+str(e))
