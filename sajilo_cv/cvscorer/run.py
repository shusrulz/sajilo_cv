import os
import json
import __init__
from flask import Flask, request,jsonify
from flask_cors import CORS
from helper import PreprocessData,NLTKHelper
from rankword2vec import VectorScorer
from configuration import tempStorage
app = Flask(__name__)
CORS(app, headers="X-CSRFToken, Content-Type")

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

preprocessor_obj = PreprocessData()
helper_obj = NLTKHelper(["experience_type_a:{<CD><N.+><IN><J.+|V.+>?<N.+><IN>?<N.+>+}",
                         "experience_type_b:{<N.+>+<\(>*<CD><N.+><N.+>?<IN>?<J.+|VBG>?<N.+>?<\)>*}"
                         "experience_type_c:{<N.+>+<\:>*<CD><N.+><N.+>?<IN>?<J.+|VBG>?<N.+>?<N.+>*?}"])
score_generator_obj= VectorScorer()

def calculate_score(file,job_descriptions,dolower = False):
    cleaned_tokenized_resume=score_generator_obj.prepare_text(file,dolower=dolower)

    final_data = preprocessor_obj.preprocess_text(cleaned_tokenized_resume)
    resume_vector = score_generator_obj.create_vector(final_data)
    my_score = {}

    for job_id,job_description in job_descriptions.items():
        job_text = score_generator_obj.prepare_text_from_string(job_description)
        final_job = preprocessor_obj.preprocess_text(job_text)
        job_vector = score_generator_obj.create_vector(final_job)
        vec_sim = score_generator_obj.calculate_similarity(job_vector,[resume_vector])
        try:
            my_score[job_id] = int(vec_sim*100)
        except Exception as e:
            print(e,job_vector,job_description)
    return my_score

@app.route("/generatescore",methods = ["POST","GET"])
def getScore():

    resume = request.files.get('resume')
    if resume:
        filename = resume.filename
        file = tempStorage + '/' + filename
        resume.save(file)
        job_descriptions = json.loads(request.form.get('jobs'))
        response = calculate_score(file,job_descriptions)
        return jsonify(response)

@app.route("/",methods=['GET'])
def check_site():
    return jsonify('kwasha')
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8002, debug=True)
