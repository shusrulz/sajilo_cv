class SVMClassifier:
    from PreProcessor import PreProcessor
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import StandardScaler, LabelEncoder
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.metrics import confusion_matrix, accuracy_score
    from sklearn import svm
    import pickle

    dataProcessor = PreProcessor()
    X = dataProcessor.predArray
    y = dataProcessor.train_y

    # Creating the Training and Test set from data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.1, random_state = 10)

    # Feature Scaling
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    classifier = svm.LinearSVC()
    classifier.fit(X, y)

    y_pred = classifier.predict(X_test)

    encode = dataProcessor.encoder
    accuracy = accuracy_score(y_test, y_pred)
    print(accuracy)

    cm = confusion_matrix(y_test, y_pred)
    print(cm)

    # # save the model to disk
    # filename = '../Models/SVMClassifier.sav'
    # pickle.dump(classifier, open(filename, 'wb'))