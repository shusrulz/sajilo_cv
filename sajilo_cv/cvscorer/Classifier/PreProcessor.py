class PreProcessor:
    #Importing Libraries
    import numpy as np
    import pandas as pd
    
    
    from sklearn.preprocessing import LabelEncoder, StandardScaler
    from sklearn.feature_extraction.text import CountVectorizer

    # def __init__(self, path):        
    #     self.path = path

    def removeComma(convertedDesignTolist):

        newList = []

        for temp in convertedDesignTolist:
            strings = [str(j) for j in temp]
            a_string = "".join(strings)
            an_integer = int(a_string)
            newList.append(an_integer)
        
        return newList
    
    dataset = pd.read_csv('../data/final.csv')

    predictors = dataset[['designation']]
    target = dataset[['heirarchy']]

    encoder = LabelEncoder()
    encoder.fit(target['heirarchy'])
    train_y = encoder.transform(target['heirarchy'])

    vecto = CountVectorizer()
    convertedDesign = vecto.fit_transform(predictors['designation'])

    # print(convertedDesign.toarray())

    convertedDesignToarray = convertedDesign.toarray()

    convertedDesignTolist = convertedDesignToarray.tolist()
    # print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

    # print(convertedDesignTolist)
    newList = removeComma(convertedDesignTolist)

    # print(newList)
    diction = {'designation':newList} #StandardScaler doesn't help to increase model performance here
    df = pd.DataFrame(diction)
    # print(df)

    predArray = np.array(df)
    # print(len(predArray))
    # print(train_y)