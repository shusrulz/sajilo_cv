import logging
import numpy as np

from scipy import spatial
import nltk
from nltk.tokenize import word_tokenize


from gensim.models import Word2Vec

from sajilo_cv.datareader import *
from sajilo_cv import configuration as cfg

logger = logging.getLogger(__name__)

# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('file.log')
c_handler.setLevel(logging.DEBUG)
f_handler.setLevel(logging.ERROR)

# Create formatters and add it to handlers
c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)

####################Author:Subodh Shakya#########################
class VectorScorer():
    '''
    scores the resume using the
    word2vec gensim model
    '''
    def __init__(self):
        '''
        This constructor loads the word2vec model that
        we will be using for vectorizing of embedding the
        contents of the resume and job description
        '''
        self.word2vec = Word2Vec.load(cfg.word2vec_model_path)


    def create_vector(self,token_list):
        '''
        Takes the list of the tokenized
        resumes and returns their embedded vectors
        :param resume_token_list:type list of list
        :return:cv_word2vec :type list of list
        '''
        doc_word2vec = list()
        
        tokenized = [word_tokenize(token) for token in token_list]
        flat_list = [item for sublist in tokenized for item in sublist]
        for token in flat_list:
            try:
                doc_word2vec.append(self.word2vec[token])
            except KeyError as K:
                logger.exception(msg=K)
        doc_vectors = (np.mean(doc_word2vec,axis=0))
        return doc_vectors

    def calculate_similarity(self,job_vector,cv_vector):
        '''
        this function makes the use of cosine distance to measure the similarity
        between job and cv
        :param job_description_vector:type:list
        :param cv_vectors_list :type: list of list
        :return: score_list
        '''
        score = ((1 - spatial.distance.cosine(job_vector,cv_vector)))
        if score < 0:
            score = 0
        elif score >100:
            score = 95
        return score





