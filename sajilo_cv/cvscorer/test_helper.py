import unittest
from helper import NLTKHelper, PreprocessData

preprocess = PreprocessData()

class TestHelperMethods(unittest.TestCase):
    def test_preprocess_text(self):
        self.assertEqual(preprocess.preprocess_text('We want AI developers who have high expertise in natural language processing',True),['We', 'want', 'AI', 'developers', 'high', 'expertise', 'natural', 'language', 'processing'])
        self.assertIsNotNone(preprocess.preprocess_text('We want AI developers who have high expertise in natural language processing',True))

# class TestNltkHelperMethods(unittest.TestCase):
#     def test_experience_finder(self):
#         test = nltkobj.experiencefinder('6 years of professional experience in design, development, and testing of Front-End web-based applications in E-Commerce and Banking Domains.')
