# from gensim.models import Word2Vec
# model = Word2Vec.load('models/word2vec')
# print(model.similarity('python','company'))

# import os
#
# filename = 'home/subodh/infodev/sub.jpg'
#
# file,ext = os.path.splitext(filename)
# print(ext)
# import re
# text = 'hello how are (you) @12 dfe2 3r cfde'
# subs_text = re.sub('\W',' ',text)
# print(subs_text)

# import pandas as pd
# dataframe = pd.read_csv('detail1.csv')
# i=0
# for jobdescription in dataframe['Extra Info'].values:
#   with open('cvdata/SoftwareDeveloper/job_desc_{}.txt'.format(i),'w') as file:
#     try:
#       file.write(jobdescription)
#       i+=1
#     except:
#       pass

# a =set()
# item = "subodh"
#
# a.add(item)
# print(a)
list_of_path =['/home/infodev/PycharmProjects/ResumeRankerNEW/ResumeRanker/codes4training/cvdata/SoftwareDeveloper/Abhijit-Palve.pdf',
               '/home/infodev/PycharmProjects/ResumeRankerNEW/ResumeRanker/codes4training/cvdata/SoftwareDeveloper/Abraham-Jeyakumar.pdf',
               '/home/infodev/PycharmProjects/ResumeRankerNEW/ResumeRanker/codes4training/cvdata/SoftwareDeveloper/Bob-Bao.pdf',
               '/home/infodev/PycharmProjects/ResumeRankerNEW/ResumeRanker/codes4training/cvdata/SoftwareDeveloper/Brian-Ortloff.pdf',
               '/home/infodev/PycharmProjects/ResumeRankerNEW/ResumeRanker/codes4training/cvdata/SoftwareDeveloper/Diarmaid-Hanly.pdf']

jd= '''Under Armour is the chosen brand of this generation of athletes... and the athletes of tomorrow. We're about performance - in training and on game day, in blistering heat and bitter cold. Whatever the conditions, whatever the sport, Under Armour delivers the advantage athletes have come to demand. That demand has created an environment of growth. An environment where building a great team is vital. An environment where doing whatever it takes is the baseline and going above and beyond to protect the Brand is commonplace. The world's hungriest athletes live by a code, a pledge to themselves and everyone else: Protect This House... I Will.

Our goal is to Build A Great Team!

Will YOU…Protect This House?!

Summary

MyFitnessPal, part of Under Armour Connected Fitness, is looking for a Software Engineer to help us improve and grow our top ranked nutrition and fitness app that’s helping our millions of users reach their health goals.

This massive community provides us with amazing opportunities to impact lives on a scale most other jobs cannot. These opportunities come with challenges, so together we work everyday to solve them.

As part of the Nutrition Core team, your work will have a large impact on the experiences delivered across all platforms.

Team dynamic is important to help us deliver quickly and effectively, so we are looking for great collaborators and engineers who enjoy having responsibility and value the investment of solving hard problems.

The following skills are a sampling of what we’re looking for:

1+ years experience with a common programming language (Scala, Ruby, C#, JavaScript, Python, etc)

Experience with containers and orchestration a plus (Docker or Kubernetes)

Experience with data stores a plus (MySQL, Mongo, Redis, Memcache)

Curious and motivated to learn, doesn’t like to leave questions unanswered

Willing to learn and collaborate to support and debug systems

Wants to work with a supportive team to achieve goals and have a positive impact

Recognizes the value in a forward-looking perspective when writing software

Sees privacy and security of our systems and data as a top priority

Passionate about being part of the brand and building something to get excited about

We recognize that not every person takes the same path to success, so if this position sounds like something you would be interested in please reach out!

At Under Armour, we are committed to providing an environment of mutual respect where equal employment opportunities are available to all applicants and teammates without regard to race, color, religion, sex, pregnancy (including childbirth, lactation and related medical conditions), national origin, age, physical and mental disability, marital status, sexual orientation, gender identity, gender expression, genetic information (including characteristics and testing), military and veteran status, and any other characteristic protected by applicable law.

Under Armour believes that diversity and inclusion among our teammates is critical to our success as a global company, and we seek to recruit, develop and retain the most talented people from a diverse candidate pool.'''