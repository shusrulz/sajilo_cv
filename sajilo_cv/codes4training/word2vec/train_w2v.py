from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from matplotlib import pyplot
from ResumeRanker.codes4training.word2vec.parse_data import W2vData
from ResumeRanker.codes4training.config import model_path

training_data = W2vData()
sentences = training_data.prepare_training_text()
# import pdb;pdb.set_trace()
print(len(sentences))

# train model
model = Word2Vec(sentences, size=100, window=2, min_count=2, workers=4)
# summarize the loaded model
# summarize vocabulary
words = list(model.wv.vocab)
# access vector for one word
# save model
model.save(model_path+'/word2vec')

X = model[model.wv.vocab]
pca = PCA(n_components=10)
result = pca.fit_transform(X)
# create a scatter plot of the projection
pyplot.scatter(result[:, 0], result[:, 1])
words = list(model.wv.vocab)
for i, word in enumerate(words):
	pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
pyplot.show()
print(len(words))