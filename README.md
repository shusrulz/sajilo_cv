## Usage
For parsing resumes
Step 1: Download en_core_web_sm by following command in terminal
    python -m spacy download_en_core_web_sm
Step 2: Download en_core_web_md by following command in terminal
    python -m spacy download_en_core_web_md
Step_3:

from sajilo_cv.cvparser.parse import Parser
parser_obj = Parser()
information_from_resume = parser_obj.resumeParser(resumepath)

For scoring resume with respect to job descriptions

from sajilo_cv.score import Scorer
scorer_obj = Scorer()

#mantain a python dictionary of jobdescriptions and job_descriptions_id
score = scorer_obj.get_score_from_resume(resumepath,job_description_json)





## Natural Language Processing
---
Sajilo_cv is a system developed mainly focusing on the extraction (parsing) of required informations from a resume of any individual. This task of information extraction is performed using wide variety of NLP techniques. For the extraction of information several supervised techniques like Named Entity Recognition (NER) and several rule based matching are applied. These extracted information will be highly essential for other tasks. The information extraction for job description set by the employers as of now is in progress.
Similarly, the other major task is the scoring of resumes according to the job description provided by the employer. This is done using using popular word2vec approach.

## Directory structure
----
    ├── resumeparser
        ├── codesfortraining
            ├── models            ----> temporary directory to hold the new models after training             
            ├── word2vec
                ├── __init__.py
                ├── config.py     ----> Contains dataset path,hyperparameters
                ├── test_model.py ----> Code for testing the models
        ├── commonfiles           ----> NER models shared by both Scorer and Parser    
            ├── Stanford NER
                ├── ner_model.ser.gz
                ├── stanford-ner.jar
        ├── cvparser             ----> Contains files used for resumeparser    
            ├── datahouse        ----> Contains the predefined files used for information filtering
                ├── language.csv
                ├── nationality_data.csv
                ├── technical_skills.csv
            ├── formatter       -----> Package for formatting and normalizing the output of the parser
                ├── __init__.py
                ├── formatdata.py
                ├── stadarizedata.py
            ├── helper          -----> Package that help for the information extraction and is called                             from mainfiles 
                ├── __init__.py
                ├── createngrams.py ----> Creates n-grams from the corpus
                ├── namedidentifyrecognition.py---> makes use of the NER model for tagging the text
                ├── poolparser.py -----> code that makes the use of datahouse to extract some                                   information like skills,languages from the documents
                ├── regexparser.py  ----> Contains some codes that makes the use of regex to extract                             phone,zipcode,links etc
                ├── secondaryparser.py ----> Codes for fallback policies
            ├── mainfiles               ---->Package that is directly relatedd to information parsing.
                ├── __init__.py
                ├── filereader.py      -----> Contains functions for plain text extration
                ├── informationparser.py ----> makes use of helper package to extract information
                ├── resumeidentifier.py -----> Contains code for identifying the resume not resume 
                ├── segmentresume.py    -----> Contains code that segment the resume 
            ├── models                  -----> Contains all the trained models used by the parser    
                ├── male_female_identifier.pickle
                ├── male_female_tokenizer.pickle
                ├── Resume_Identification_Model.h5
                ├── segement_identifier.pkl
            ├── __init__.py            
            ├── parse.py               ----> main file for parser
            ├── PARSER.md
        ├── cvscorer                ----> contains all code used for score generation.    
            ├── models              ----> contains the AI models for score calculation
            ├── __init__.py
            ├──helper.py            ----> codes helping for score generation 
            ├──ranword2vec.py       ----> makes use of word2vec to convert text into vectors
        ├── tempstorage             ----> holds the resume for temporary period
        ├── __init__.py                 
        ├── configuration.py        ----> all the PATH variables for dataset and AI models    
        ├── datareader.py           ----> Reads the CV
        ├── requirements.txt        ----> All the dependancies required for the project
        ├── run.py                  ----> Runs the api for others to consume 
        ├── file.log                ----> Holds the log of the system

-----------------
# Resume Parser

    Pipeline:
        Get Resume: accepts the resume in .pdf,.txt,.docx,.doc format
        
        1. Read Resume:
                Libraries:
                * PymuPdf: for reading pdf files
                * docx2txt: for reading docx files
                * antiword: for reading doc files
                * pytesseract: for OCR to read text from the images
        
        2. Clean Text:
                Library:
                * Regex: all non-alphanumeric characters are removed 
        
        3. Identify Resume Not Resume:
                Pipeline:
                
                    - Tokenize plain text: The process of breaking the text into tokens. Two types of tokenization: sentence tokenization,word tokenization.
                    
                    Sentence tokenization: Breaks the paragraph into list of sentences.
                    
                    Word tokenization: Breaks the text into words.
                    
                    - Feature Selection:
                    
                    Most occuring words in the resume are taken as feature and are used to create the input to the 
                    ANN
    
                Algorithm Used: ANN (Artificial neural net)
                Evaluation Metric: Confusion Matrix
                |--------------------|----------------|---------------------|               
                |                    | actual-resume  |   actual-not resume |
                |--------------------|:--------------:|:-------------------:|
                |Predicted-resume    |         47     |        3            |
                |                    |                |                     |
                |Predicted-not-resume|        4       |        46           |
                |--------------------|:--------------:|:-------------------:|
        
        4.  Extract Information:
                Pipeline:
                    - Resume-Segmentation
                        * tokenize resume on the basis of new line
                        * feature creation for headings:
                            -[word.istitle()|word.isupper(),word.islower(), word.isupper(),   word.endswith(":"),len(word) <= 3, word.lower() in possible-title-keywords]
                        * capture the index of the possible headings
                        * Put all the content lying in between two headings into earlier heading section
                        * create sections in the form of dictionary:
                            {'profile':profile section,
                            'academics':academic section,
                            'experience':experience section,
                            ....}
                            
                    -Extract Personal Information: 
                        Algorithms:
                            Conditional Random Field (Stanford NER)
                                -Extracted information:Name and Address
                            Regex
                                -Extracted information:Phone,zip,links
                            pipeline:
                                * Get-Profile-Section
                                * Tokenize
                                * Tag using NER model
                                * Gather tokens with PER and LOC
                                * Create regex rule for phone,zip,links etc
                                
                        Fallback Policy:
                            If segmentation fails perform same operation of upper
                            25% of the resume.
                        
                    - Extract Academics Information:
                        Algorithm:
                            Conditional Random Field (Stanford NER)
                            -Extracted information:University,Degree,Enrolled-date,Graduated-date,University-location
                        pipeline:
                            * Get-Academics-Section
                            * Tokenize section
                            * Tag Using NER model
                            * Gather tokens with UNI,LOC,DATE,DEG
                        
                    Fallback Policy:
                        Perform NER tagging on Whole resume and perform same operation if incase segmentation fails.
                        
                    Extract Skills:
                        Methodology:
                            Direct comparision of the n-grams created from resume to existing pools of skills
                        Pipeline:
                            * Tokenize whole resume
                            * Create 1-grams,2-grams,3-grams from tokenized text using spacy
                            * Compare n-grams to the existing pool of skills
                    
                    References:
                        All contents of the Reference segment.
                        

---                
# Resume Scorer
Scoring of the job seekers resume with the employeer job description is done with the objective of recommendation of appropriate jobs to a job seeker and appropriate candidated for a employeer. To obtain such appropriate scores with respect to job description and resumes, we have applied several new NLP techniques and the data obtained from the resumeparser module. The major NLP technique used in the cv scoring is the popular word2vec algorithm. A custom word2vec model is trained on large data of resumes and job descriptions ( currently of the IT domain) to which is used further for the calulation of word embedding of every tokens in a new resume or job description. The results from the custom word2vec models are give about 70% of the weightage for scoring whereas the remaining 30 % of the weightage is given to the information extracted from the resume and job description by the parser modeule.

---
Pipeline
--
    Gets resume and job descriptions in wide variety of formats like pdf, docx txt
    returns respective score of matcing of the job descrption and resume
-
        1. Read Resumes and Job Descriptions:
                Libraries:
                * PymuPdf: for reading pdf files
                * docx2txt: for reading docx files

                ```python 
                        def pdf_to_txt(input_file):
                            text = docx2txt.process(input_file)
                            return text
                ```

                * antiword: for reading doc files
                * pytesseract: for OCR to read text from the images
        
        2. Clean Text:
                Library used:
                * Regex: all non-alphanumeric characters are removed 
                    Approaches: 
                    1. filter out non-alphanumeric character using regex
                    2. filter stopwords for nltk library
                    3. lemmatization for getting the lemma of every tokens in the input document.
                    
        3. Preprocess Data:
                Library used:
                * nltk 
                * spacy
                * textacy
        4. Build word2vec model
                Library used:
                * gensim
                * sklearn
                * matplotlib
        5. Get word vector respresentation from word2vec model
                Library used:
                * gensim
                * numpy
                * sklearn
        6. Calculate similarity of tokens from the input resumes and job descriptions
                Library used:
                * scipy 
        7. Calculate scores based on similarity
                Library used:
                    * numpy